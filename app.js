const loanElement = document.getElementById("Loan_btn");
const bankElement = document.getElementById("Bank_btn");
const workElement = document.getElementById("Work_btn");
const laptopsElement = document.getElementById("laptops");
const buynowElement = document.getElementById("Buynow_btn");
const priceElement = document.getElementById("price");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const balanceElement = document.getElementById("balance");
const payLoanElement = document.getElementById("PayLoan_btn");
const payAmountElement = document.getElementById("payAmount");
const outstandingLoanElement = document.getElementById("outstandingLoan");
const productPictureElement = document.getElementById("product_pic");
const outstandingLoanSignElement = document.getElementById("outstandingLoanSign");


let bankBalance = 200;
let loan = 0;
let payAmount = 0;
let products = [];
let loancounter = 0;
balanceElement.innerText = bankBalance+"€";
payAmountElement.innerText = payAmount+"€";
payLoanElement.style.visibility="hidden";
outstandingLoanElement.style.visibility="hidden";
outstandingLoanSignElement.style.visibility="hidden";
outstandingLoanElement.innerText = loan+"€";

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => products = data)
    .then(products =>addproductstoMenu(products));

const addproductstoMenu = (products) => {
    products.forEach(x => addproducttoMenu(x));
    priceElement.innerText = products[0].price+"€";
    titleElement.innerText = products[0].title;
    descriptionElement.innerText = products[0].description;
    listformatter(specsElement,products[0].specs);
    productPictureElement.innerHTML = '<img src="'+"https://noroff-komputer-store-api.herokuapp.com/"+products[0].image+'"/>';
}

const addproducttoMenu = (product) => {
    const productElement = document.createElement("option");
    productElement.value = product.id;
    productElement.appendChild(document.createTextNode(product.title));
    laptopsElement.appendChild(productElement)
}

const handleProductMenuChange = e => {
    const selectedProduct = products[e.target.selectedIndex];
    priceElement.innerText = selectedProduct.price+"€";
    titleElement.innerText = selectedProduct.title;
    descriptionElement.innerText = selectedProduct.description;
    listformatter(specsElement,selectedProduct.specs);
    fetch("https://noroff-komputer-store-api.herokuapp.com/"+selectedProduct.image)
    .then(res => {
        if (res.status === 404) {
            throw new Error("Not Found"),
            productPictureElement.innerText = "Heute habe ich leider kein Foto fuer dich. -Heidi Kluemm"
        }
        else {
            productPictureElement.innerHTML = '<img id="catcher" src="'+"https://noroff-komputer-store-api.herokuapp.com/"+selectedProduct.image+'"/>'
        }
    })
       
    
}

const handleLoanBtn = e => {
    let text;
    let maxloan=bankBalance*2;
    if (loan== 0 && loancounter == 0 ){
        text="Please enter the amount of Loan you´d like to get. Your current maximum is:"
        loan = prompt(text, maxloan);
        while (true) {
            if (parseInt(loan)>maxloan){
                text="Excuse me, the entered amount should not be more than the double of you current bank balance. Try again!"
                loan=prompt(text,maxloan);
            }
            else if (parseInt(loan)<0) {
                text="We appreciate that you like to send us money! But we prefer getting the money for selling laptops. Try again!";
                loan=prompt(text,maxloan);
            }
            else if (isNaN(loan)) {
                text="We may also write you a 'letter' but our special skill relates to numbers. Try again!";
                loan=prompt(text,maxloan);
            }
            else if (parseInt(loan)>0 && loan<=maxloan) {
                gotALoan();
                break
            }
            else {
                loan=0;
                break
            }
        }
    } else if(loancounter>0){
        window.alert("We are giving you the loan to buy computers not to party hard!\n Please buy one of our fabulous products.")
    } else {
        window.alert("You still havent paid your loan back. We have your adress, so hurry up!")
    }
}

function gotALoan(){
    loancounter+=1
    bankBalance+=parseInt(loan)
    balanceElement.innerText = bankBalance+"€";
    payLoanElement.style.visibility = "visible";
    outstandingLoanElement.innerText = loan+"€";
    outstandingLoanElement.style.visibility = "visible";
    outstandingLoanSignElement.style.visibility="visible";
}

const handleBankBtn = e => {
    let percentage = 0.1;
    let difference;

    if (loan>0) {
        difference = parseInt(loan)-(payAmount*percentage);
    }
    else{
        difference=0;
    }


    if (loancounter > 0 && difference > 0) {
        loan=difference;
        bankBalance+=payAmount*(1-percentage);
    }
    else if (loancounter > 0 && difference <= 0) {
        loan=0;
        bankBalance=bankBalance + (payAmount*(1-percentage)) - difference;
        loancounter=0;
    }
    else {
        bankBalance+=payAmount;
    }
    bankTransfer();
}

function bankTransfer(){
    balanceElement.innerText = bankBalance+"€";
    payAmount = 0;
    payAmountElement.innerText = payAmount+"€";
    outstandingLoanElement.innerText = loan+"€";
    if (parseInt(loan)<=0) {
        outstandingLoanElement.style.visibility = "hidden";
        payLoanElement.style.visibility = "hidden";
        outstandingLoanSignElement.style.visibility="hidden";
    }    
}

const handleWorkBtn = e => {
    payAmount+=100;
    payAmountElement.innerText = payAmount+"€";
}

const handlePayLoanBtn = e => {
    if (payAmount<parseInt(loan)) {
        loan-=payAmount
        payAmount = 0;
    }
    else if (payAmount==parseInt(loan)){
        loan=0;
        payAmount=0;
        loancounter = 0;
        payLoanElement.style.visibility = "hidden";
        outstandingLoanElement.style.visibility = "hidden";
        outstandingLoanSignElement.style.visibility="hidden";
    }
    else {
        payAmount-=loan;
        loan=0;
        payLoanElement.style.visibility = "hidden";
        outstandingLoanElement.style.visibility = "hidden";
        outstandingLoanSignElement.style.visibility="hidden";
        loancounter = 0;
    }


    payAmountElement.innerText = payAmount+"€";
    outstandingLoanElement.innerText = loan+"€";
}

const handleBuyNowBtn = e => {
    if (bankBalance>= parseInt(priceElement.innerText)){
        bankBalance-= parseInt(priceElement.innerText);
        loancounter=0; 
        balanceElement.innerText=bankBalance+"€";
        window.alert("You are now an owner of a fresh new laptop!")
    }
    else {
        window.alert("Work has never been easier, so move your godda...\n and get some money!")
    }
}

laptopsElement.addEventListener("change", handleProductMenuChange);
loanElement.addEventListener("click",handleLoanBtn);
bankElement.addEventListener("click",handleBankBtn);
workElement.addEventListener("click",handleWorkBtn);
payLoanElement.addEventListener("click",handlePayLoanBtn);
buynowElement.addEventListener("click",handleBuyNowBtn);


function listformatter(Element,text){
    Element.textContent = '';
    text.forEach((item) => {
        let li = document.createElement("li");
        li.innerText = item;
        Element.appendChild(li);
    })
}